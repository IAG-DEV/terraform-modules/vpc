# VPC

## Usage

```
module "vpc" {
  source = "github.com/terraform-community-modules/tf_aws_vpc?ref=v1.0.0"

  aws_access_key  = "${var.aws_access_key}"
  aws_secret_key  = "${var.aws_secret_key}"
}
```